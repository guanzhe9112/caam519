#! /usr/bin/env python

import os
sizes = []
times1 = []
times2 = []
for k in range (5):
    Nx = 10 * 2**k
    modname1 = 'perf_linear_ilu%d' % k
    modname2 = 'perf_linear_gamg%d' % k
    options1 = [ '-da_grid_x ' , str(Nx), '-da_grid_y ' , str(Nx),'-ksp_type gmres', '-pc_type ilu','-log_view ',':%s.py:ascii_info_detail' % modname1]
    options2 = [ '-da_grid_x ' , str(Nx), '-da_grid_y ' , str(Nx),'-ksp_type gmres', '-pc_type gamg','-log_view ',':%s.py:ascii_info_detail' % modname2]
    sizes.append(Nx ** 2)
    
    os.system('./bin/ex5 '+' '.join(options1)) 
    perfmod1 = __import__(modname1)
    times1.append(perfmod1.Stages['Main Stage']['KSPSolve'][0]['time']) 
   
    os.system('./bin/ex5 '+' '.join(options2)) 
    perfmod2 = __import__(modname2)
    times2.append(perfmod2.Stages['Main Stage']['KSPSolve'][0]['time']) 
    
print zip(sizes , times1)
print zip(sizes , times2)

from pylab import legend , plot , loglog , show , title , xlabel , ylabel 
plot(sizes, times1,'r.-',label="ILU")
plot(sizes, times2,'b.-',label="GAMG")
title( 'GMRES ex5 ')
xlabel('Problem Size $N$')
ylabel( 'Time (s) ')
legend(loc='upper left') 
show()

loglog(sizes, times1, "r.-",label="ILU")
loglog(sizes, times2, 'b.-',label='GAMG')
title( 'GMRES ex5 ') 
xlabel('Problem Size $N$') 
ylabel( 'Time (s) ')
legend(loc='upper left')
show()

